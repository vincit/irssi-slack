#!/usr/bin/perl -w
use Irssi;

Irssi::signal_add("event privmsg" => sub {
    my ($server, $data, $nick, $address) = @_;
    my ($target, $message) = split(/ :/, $data,2);

    my $orig = $message;
    $message =~ s/:simple_smile:/:)/g;
    $message =~ s/:slightly_smiling_face:/:)/g;
    $message =~ s/:stuck_out_tongue:/:P/g;
    $message =~ s/:wink:/;)/g;
    $message =~ s/:smile:/:)/g;
    $message =~ s/:smiley:/:)/g;
    $message =~ s/:disappointed:/:\//g;
    $message =~ s/:heart:/❤/g;
    $message =~ s/:open_mouth:/:-o/g;
    $message =~ s/:laughing:/XD/g;
    $message =~ s/:grin:/:-]/g;
    $message =~ s/:anguished:/:-O/g;
    $message =~ s/:_b:/_b/g;
    $message =~ s/:cry:/:'-(/g;
    $message =~ s/:neutral_face:/:-|/g;
    $message =~ s/:thumbsup:/b^.^d/g;
    $message =~ s/:stuck_out_tongue_winking_eye:/;P/g;
    $message =~ s/:confused:/O.o/g;
    $message =~ s/:tm:/™/g;
    $message =~ s/:\+1:/+1/g;
    $message =~ s/:raised_hands:/\\o\//g;
    $message =~ s/:arrow_right:/→/g;
    $message =~ s/:expressionless:/:|/g;
    $message =~ s/:star:/★/g;
    $message =~ s/:coffee:/☕/g;

    if ($orig ne $message) {
	Irssi::signal_emit('event privmsg', ($server, "$target :$message", $nick, $address));
	Irssi::signal_stop();
    }
});
