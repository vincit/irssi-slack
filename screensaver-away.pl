#!/usr/bin/perl -w
use Irssi;

my $active = 0;

my $poll_timer;
my $was_active = -1;

sub is_saver_active() {
    my $state = `gnome-screensaver-command -q 2>/dev/null`;
    chomp $state;
    return $state eq "The screensaver is active";
}

sub check_saver {
    my $is_now_active = is_saver_active();
    if ($is_now_active != $was_active) {
	$was_active = $is_now_active;
	foreach my $server (Irssi::servers()) {
	    $server->command($is_now_active ? "QUOTE AWAY :screen locked" : "QUOTE AWAY");
	}
    }
}

sub register_timer {
  if (defined $poll_timer) {
    Irssi::timeout_remove($poll_timer);
  }
  $poll_timer = Irssi::timeout_add(5000, 'check_saver', '');
}

register_timer();
