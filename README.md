# README #

This repository is for irssi scripts specific for integrating with Slack.

* fix-slack-smileys - replaces :slack_smileys: with more commonly recognized emoticons such as :) in incoming messages

### Usage ###

* mkdir -p ~/.irssi/scripts/autorun; ln -s ~/src/irssi-slack/fix-slack-smileys.pl ~/.irssi/scripts/{,autorun}
* /script load fix-slack-smileys
